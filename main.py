from fastapi import FastAPI, HTTPException, Header, Request

app = FastAPI()


def calculate_risks(sub_object):
    policy = {
        # sub.name: [sub.http-method, sub.useragent]
        'alice': {
                'user_method': 'GET',
                'user_agent': 'Mozilla/5.0 (X11; Linux x86_64; rv:109.0) Gecko/20100101 Firefox/114.0',
                'user_source_ip': '127.0.0.1'
                },
        'bob':  {
                'user_method': 'GET',
                'user_agent': 'Mozilla/5.0 (X11; Linux x86_64; rv:109.0) Gecko/20100101 Firefox/114.0',
                'user_source_ip': '127.0.0.1'
                },
        'robot': {
                'user_method': 'GET', 
                'user_agent': 'curl/8.1.2',
                'user_source_ip': '127.0.0.1'
                }
    }
    weights = {
            'user_agent': 1/3,
            'user_method': 1/3,
            'user_source_ip': 1/3
            }

    if len(policy['alice']) != len(weights):
        return "Внутренняя ошибка. Размер полей в политике и весах отличается"

    try:
        policy_for_user = policy[sub_object['sub_name']]
        sub_attributes = sub_object.copy()
        del sub_attributes[next(iter(sub_attributes))]
    except Exception as e:
        print(e)
        return "Внутренняя ошибка. Указанный пользователь не существует"

    anomaly_dict = {k: sub_attributes[k] for k in sub_attributes if k not in policy_for_user or policy_for_user[k] != sub_attributes[k]}
    risk = 0
    for key in anomaly_dict.keys():
        risk += weights[key]

    return anomaly_dict, round(risk,4)


@app.get("/protected")
@app.post("/protected")
def protected_resource(name: str, request: Request):

    sub_object = {
                'sub_name': name,
                'user_method': request.method,
                'user_agent': request.headers['user-agent'],
                'user_source_ip': request.client.host
    }

    return calculate_risks(sub_object)

if __name__ == "__main__":
    import uvicorn
    uvicorn.run(app, host="0.0.0.0", port=8000)
